package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class TelerikAcademyPage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("academy.url");
    final String LOGIN_EMAIL = Utils.getConfigPropertyByKey("login.email");
    final String LOGIN_PASSWORD = Utils.getConfigPropertyByKey("login.password");

    public TelerikAcademyPage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void NavigateToQACourseViaCard(){
        actions.waitForElementVisible("academy.DismissCookieButton", 10);
        actions.clickElement("academy.AlphaAnchor");
        actions.clickElement("academy.DismissCookieButton");
        actions.clickElement("academy.QaGetReadyLink");
        actions.clickElement("academy.SignUpNavButton");
    }

    public void AssertQAAcademySignupPageNavigated(){

        actions.assertNavigatedUrl("academy.QASignUpUrl");
    }

    public void NavigateToHomePage() {
        getDriver().get(this.getUrl());
    }

    public void NavigateToLoginPage() {
        getDriver().get(this.getUrl());
        actions.waitForElementVisible("academy.LoginButton", 10);
        actions.clickElement("academy.LoginButton");
    }

    public void Login() {
        actions.waitForElementVisible("academy.LoginEmailInput", 10);
        actions.typeValueInField(LOGIN_EMAIL, "academy.LoginEmailInput");
        actions.typeValueInField(LOGIN_PASSWORD, "academy.LoginPasswordInput");
        actions.clickElement("academy.LoginAccountButton");
    }

    public void CreateTopic(String title, String body) {
        actions.waitForElementVisible("academy.CreateNewTopicButton", 10);
        actions.clickElement("academy.CreateNewTopicButton");
        actions.clearValueFromField("academy.CreateTopicTitleInput");
        actions.typeValueInField(title, "academy.CreateTopicTitleInput");
        actions.clearValueFromField("academy.CreateTopicBodyInput");
        actions.typeValueInField(body,"academy.CreateTopicBodyInput");
        actions.clickElement("academy.CreateTopicButton");
    }
}