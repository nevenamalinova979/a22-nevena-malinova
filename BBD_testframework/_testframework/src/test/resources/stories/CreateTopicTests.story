Meta:
@createTopic

Narrative:
As a user in Telerik academy
I want to create topic in the forum
So that I can connect with other users

Scenario: Create topic in stage forum

When Click academy.LoginButton element
And Click academy.LoginEmailInput element
And Type nevenamalinova979@gmail.com in academy.LoginEmailInput field
And Type mypassword in academy.LoginPasswordInput field
And Click academy.LoginAccountButton element
And Element academy.MessageIcon is present
And Click academy.CreateNewTopicButton element
And Element academy.CreateTopicTitleInput is present
And Type JBehave homework in academy.CreateTopicTitleInput field
And Element academy.CreateTopicBodyInput is present
And Type JBehave test in academy.CreateTopicBodyInput field
And Element academy.CreateTopicButton is present
Then Click  academy.CreateTopicButton element
