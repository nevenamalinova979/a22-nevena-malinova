Meta:
@qa

Narrative:
As a future QA
I want to find info about Alpha QA Academy
So that I can join the course
!--Narrative is not mandatory, но е в основата на описателносста на функционалността. В ролата на какъв, какво искам да
!-- направя, за да постигна някаква цел. Дава ни инфо каква е крайната цел на потребителя.И за един нетехнически човек дава повече
!-- контекст.

Scenario: Find QA Academy in Telerik Academy website

When Click academy.AlphaAnchor element
And Click academy.DismissCookieButton element
And Click  academy.QaGetReadyLink element
And Click academy.SignUpNavButton element

