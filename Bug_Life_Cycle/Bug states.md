|*State*        | **Action**  |
| --------------| ----------- |
 *New*              | Found an issue / defect / bug      |
| *Assigned*        | A newly created defect is assigned to the development team for working on the defect. This is assigned by the project lead or the manager of the testing team to a developer.      |
| *Open*            |  The developer starts the process of analyzing the defect and works on fixing it, if required.       |
| *Fixed*           | The developer finishes the task of fixing a defect by making the required changes       |
|*Pending Retest*   | After fixing the defect, the developer assigns the defect to the tester for retesting the defect at their end, and till the tester works on retesting the defect.       |      
| *Retest*          | When the tester starts the task of working on the retesting of the defect to verify if the defect is fixed accurately by the developer as per the requirements or not.      |
| *Reopen*          | When any issue persists in the defect then it will be assigned to the developer again for testing and the status of the defect gets changed to ‘Reopen’.       |
| *Verified*        |  When the tester does not find any issue in the defect after being assigned to the developer for retesting then the status of the defect gets assigned to ‘Verified’.       |
| *Closed*          | Closed once when issue is verified as fixed.|
| *Duplicate*       |  the  issue is already logged by someone else or postponed when it decided when it is decided to be fixed on a later stage.                                                
| *Rejected*        |   Rejected if it’s not a valid defect or assign if it is confirm that is valid defect.       
| *Deferred*        | When the developer feels that the defect is not of very important priority and it can get fixed in the next releases.       |
| *Not a bug*       | When the defect does not have an impact on the functionality of the application           |

