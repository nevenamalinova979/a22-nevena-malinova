import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class CreateTopicTest {
    private static final String TELERIK_STAGE_FORUM_URL = "https://stage-forum.telerikacademy.com";
    private static final String EMAIL = "nevenamalinova979@gmail.com";
    private static final String PASSWORD = "mypassword";
    private WebDriver driver;


    @BeforeClass
    public static void testClassInitialization(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nevena\\Documents\\JAVA\\Module 3\\ChromeDriver\\chromedriver.exe");
    }
    @Before
    public void testInitialization(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get(TELERIK_STAGE_FORUM_URL);
        logIn();
        createTopic();
    }

    @After
    public void testCleanUp(){
        driver.close();
    }

    @Test
    public void TelerikForumPageNavigated_When_TelerikOpenedAndCreteTopic() {
        WebElement typeTitleBox = driver.findElement(By.xpath("//input[@id='reply-title']"));
        typeTitleBox.sendKeys("Homework Selenium Web Driver Part 1");
        WebElement typeBox = driver.findElement(By.xpath("//textarea[@placeholder='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']"));
        typeBox.sendKeys("Create topic");
        WebElement buttonCreateTopic = driver.findElement(By.xpath("//span[contains(text(),'Create Topic')]"));
        buttonCreateTopic.click();
    }

       @Test
    public void TelerikForumResultNavigated_When_CreateCategorizedTopicWithOptionalTag() {
        WebElement typeTitleBox = driver.findElement(By.xpath("//input[@id='reply-title']"));
        typeTitleBox.sendKeys("Homework Selenium Web Driver Part 1 test");
        WebElement typeBox = driver.findElement(By.xpath("//textarea[@placeholder='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']"));
        typeBox.sendKeys("Creating categorized topic with optional tag");
        WebElement uncategorizedBox = driver.findElement(By.xpath("//div[@class='select-kit-header single-select-header combo-box-header ember-view']"));
        uncategorizedBox.click();
        WebElement alphaPreparation = driver.findElement(By.xpath("//div[contains(text(),'Welcome to the question space for your Alpha Progr')]"));
        alphaPreparation.click();
        WebElement selectAtLeast1Item = driver.findElement(By.xpath("//span[text()='Select at least 1 item.']"));
        selectAtLeast1Item.click();
        WebElement searchBox = driver.findElement(By.xpath("//input[@placeholder='Search...']"));
        searchBox.sendKeys("preparation" + Keys.ENTER);
        WebElement preparation1 = driver.findElement(By.xpath("//div[@class='select-kit-header single-select-header combo-box-header mini-tag-chooser-header ember-view']"));
        preparation1.click();
        WebElement buttonCreateTopic = driver.findElement(By.xpath("//span[contains(text(),'Create Topic')]"));
        buttonCreateTopic.click();
    }
    @Test
    public void TelerikForumResultNavigated_When_CreateUncategorizedTopicWithOptionalTag() {
        WebElement typeTitleBox = driver.findElement(By.xpath("//input[@id='reply-title']"));
        typeTitleBox.sendKeys("Homework Selenium Web Driver Part 1 test3");
        WebElement typeBox = driver.findElement(By.xpath("//textarea[@placeholder='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']"));
        typeBox.sendKeys("Creating uncategorized topic with optional tag");
        WebElement selectAtLeast1Item = driver.findElement(By.xpath("//span[text()='optional tags']"));
        selectAtLeast1Item.click();
        WebElement searchBox = driver.findElement(By.xpath("//input[@placeholder='Search...']"));
        searchBox.sendKeys("preparation" + Keys.ENTER);
        WebElement preparation1 = driver.findElement(By.xpath("//div[@class='select-kit-header single-select-header combo-box-header mini-tag-chooser-header ember-view']"));
        preparation1.click();
        WebElement buttonCreateTopic = driver.findElement(By.xpath("//span[contains(text(),'Create Topic')]"));
        buttonCreateTopic.click();
    }


    private void logIn() {
        WebElement buttonLogIn = driver.findElement(By.xpath("//span[@class='d-button-label']"));
        buttonLogIn.click();
        WebElement emailBox = driver.findElement(By.xpath("//input[@id='Email']"));
        emailBox.sendKeys(EMAIL);
        WebElement passWordBox = driver.findElement(By.xpath("//input[@id='Password']"));
        passWordBox.sendKeys(PASSWORD);
        WebElement signInBox = driver.findElement(By.xpath("//button[@id='next']"));
        signInBox.click();
    }
    private void createTopic() {
        WebElement buttonNewTopic = driver.findElement(By.xpath("//button[@id ='create-topic']"));
        buttonNewTopic.click();
    }
}
