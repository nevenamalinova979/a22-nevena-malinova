package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GooglePage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("google.url");

    public GooglePage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void SearchAndOpenFirstResult(String searchTerm){
        AgreeWithConsent();
        actions.typeValueInField(searchTerm, "search.Input");
        actions.waitForElementVisible("search.Button",10);
        actions.clickElement("search.Button");
        actions.waitForElementVisible("search.Result",10);
        actions.clickElement("search.Result");
    }

    public void AgreeWithConsent(){
        WebDriver driver = getDriver();
        WebElement iFrame = driver.findElement(By.xpath("//div[@id ='cnsw']")).findElement(By.tagName("iframe"));
        driver.switchTo().frame(iFrame);
        actions.clickElement("search.IntroAgreeButton");
        driver.switchTo().defaultContent();
    }
}
