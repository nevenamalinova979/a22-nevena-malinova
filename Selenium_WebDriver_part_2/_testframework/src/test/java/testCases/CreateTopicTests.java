package testCases;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import pages.TelerikAcademyPage;

import java.text.SimpleDateFormat;
import java.util.Date;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class CreateTopicTests extends BaseTest  {
    TelerikAcademyPage academy = new TelerikAcademyPage(actions.getDriver());
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private String current = formatter.format(new Date());
    @Test
    public void test_1_Page_Navigated_When_Telerikacademy_Opened() {
        academy.NavigateToLoginPage();
        Assert.assertTrue(actions.getDriver().getCurrentUrl().contains("https://auth.telerikacademy.com/account/login"));
    }

    @Test
    public void test_2_Assertion_Perform_Telerikacademy_Page() {
        academy.Login();
        actions.assertElementPresent("academy.MessageIcon");
    }

    @Test
    public void test_3_Create_Topic() {
        String title = "Selenium WebDriver Homework part 2" + current;
        academy.CreateTopic(title, "Create Topic - Test 1"  + current);
        actions.waitForDocumentTitleIs(title + " - Telerik Academy Forum", 10);
        actions.assertDocumentTitleIsEqual(title);
    }
    @Test
    public void test_4_Create_Topic_With_Less_Minimum_Required_Symbols_In_Posting() {
        academy.NavigateToHomePage();
        academy.CreateTopic("Selenium WebDriver Homework part 2" + current, "Short");
        actions.waitForElementVisible("academy.PopupTipShortBody", 10);
        actions.assertElementPresent("academy.PopupTipShortBody");
        actions.clickElement("academy.CancelTopicButton");
        actions.waitForElementVisible("academy.ModalYesAbandon", 10);
        actions.clickElement("academy.ModalYesAbandon");
    }
    @Test
    public void test_5_Create_Topic_With_Less_Minimum_Required_Symbols_In_Title() {
        academy.CreateTopic("Two", "Selenium WebDriver Homework part 2"  + current);
        actions.waitForElementVisible("academy.PopupTipShortTitle", 10);
        actions.assertElementPresent("academy.PopupTipShortTitle");
    }
}
