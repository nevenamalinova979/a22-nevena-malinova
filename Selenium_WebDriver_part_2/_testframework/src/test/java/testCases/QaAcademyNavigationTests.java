package testCases;

import pages.GooglePage;
import pages.TelerikAcademyPage;
import org.junit.Test;

public class QaAcademyNavigationTests extends BaseTest {
	String searchCriterion= "Telerik Academy";

	@Test
	public void navigateToCourseFromGoogleSearch() {
		GooglePage google = new GooglePage(actions.getDriver());
		google.SearchAndOpenFirstResult(searchCriterion);
		TelerikAcademyPage academy = new TelerikAcademyPage(actions.getDriver());
		academy.NavigateToQACourseViaCard();
		academy.AssertQAAcademySignupPageNavigated();
	}
}
