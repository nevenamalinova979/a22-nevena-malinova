package testCases;

import org.junit.Test;
import pages.GooglePage;
import pages.TelerikAcademyPage;

public class SearchInGoogleTests extends BaseTest {
	String searchCriterion= "Telerik Academy";

	@Test
	public void simpleGoogleSearch() {
		GooglePage google = new GooglePage(actions.getDriver());
		google.SearchAndOpenFirstResult(searchCriterion);
		TelerikAcademyPage telerik = new TelerikAcademyPage(actions.getDriver());
		telerik.NavigateToQACourseViaCard();
		actions.assertNavigatedUrl("academy.QASignUpUrl");
	}
}
