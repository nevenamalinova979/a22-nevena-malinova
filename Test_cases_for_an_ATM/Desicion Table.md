


| **Conditions**                                 |**Rule1**|**Rule2**|**Rule3**|**Rule4**|**Rule5**|**Rule6**|**Rule7**|**Rule8**|
|------------------------------------------------|---------|---------|---------|---------|---------|---------|---------|---------|
| Card is inserted and read                      |     Y   |    Y    |    Y    |    Y    |    N    |    N    |    N    |    N    |
| PIN entered is correct                         |     Y   |    Y    |    N    |    N    |    Y    |    Y    |    N    |    N    |
| Requested amount is available in card account  |     Y   |    N    |    Y    |    N    |    Y    |    N    |    Y    |    N    |
| **Action**                                     | | | | | | | | |
| Funds are withdrawn                            |     Y   |    N    |     N   |    N    |    N    |    N    |    N    |    N    |           

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


| **Conditions**                                 |**Rule1**|**Rule2**|**Rule3**|**Rule4**|**Rule5**|
|------------------------------------------------|---------|---------|---------|---------|---------|
| Card is inserted and read                      |     Y   |    Y    |    Y    |    Y    |    N    |   
| PIN entered is correct                         |     Y   |    Y    |    N    |    N    |    ~    |  
| Requested amount is available in card account  |     Y   |    N    |    Y    |    N    |    ~    |  
| **Action**                                     |         |         |         |         |         |
| Funds are withdrawn                            |     Y   |    N    |     N   |    N    |    N    |          

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


| **Conditions**                                 |**Rule1**|**Rule2**|**Rule3**|**Rule5**|
|------------------------------------------------|---------|---------|---------|---------|
| Card is inserted and read                      |     Y   |    Y    |    Y    |    N    |   
| PIN entered is correct                         |     Y   |    Y    |    N    |    ~    |  
| Requested amount is available in card account  |     Y   |    N    |    ~    |    ~    |  
| **Action**                                     |         |         |         |         |
| Funds are withdrawn                            |     Y   |    N    |     N   |    N    |       

/////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

| **Conditions**                                 |**Rule1**|**Rule2**|**Rule3**|**Rule5**|
|------------------------------------------------|---------|---------|---------|---------|
| Card is inserted and read                      |     Y   |    Y    |    Y    |    N    |   
| PIN entered is correct                         |     Y   |    Y    |    N    |    N/A   |  
| Requested amount is available in card account  |     Y   |    N    |    N    |    N/A   |  
| **Action**                                     | | | | | | | | |
| Funds are withdrawn                            |     Y   |    N    |     N   |    N    |   


