# **Test cases using EVP**

## **Prerequisites:**
- ATM machine (with working screen buttons, receipt printer, card reader,display screen, cash dispenser, deposit slot, keypad)
- Card - АТМ accepts all types cards (debit, credit, prepaid, Visa, MasterCard, Maestro, American express, Amex etc.)
- ATM works for 24*7.

## **Test cases:**

### **Withdrawal amount from ATM:**
1. Withdrawal less or equal money account balance - valid class
2. Withdraw more money than account balance - invalid class


 ### **Using an ATM from the same card per day:**
1. Using the same card at the same ATM from 0 to 3 times per day - valid class
2. Using the same card at the same ATM over 3 times per day - invalid class


### **Withdrawal from ATM per day:**
1. Withdraw an amount from the same card per day from 0 to 3 times - valid class
2. Withdraw an amount from the same card per day over 3 times - invalid class 

### **Paying utility bills (e.g. gas, water, electricity, etc.)**
1. Тhe trader must be registered in the system ePay using the active service B-Pay - valid class
2. Тhe trader must not  be registered in the system using the active service B-Pay - invalid class

### **Providing an opportunity for charity donations**
1. Availability of funds on account - valid class
2. Lack of availability of funds on account - invalid class

### **Personal Loan through ATM**
1. The person must be 18-60 years - valid class
2. The person must be <=17 years - invalid class
3. The person must be >=61 years - invalid class

# **Test cases using BVA**

### **Withdrawal from ATM only banknotes of  10, 20, 50, 100**

1. Boundary Value = 9	     System should NOT accept
2. Boundary Value = **10**	     System **should accept**
3. Boundary Value = 11	     System should Not accept 
4. Boundary Value = 19	     System should Not accept
5. Boundary Value = **20**	     System **should accept**
6. Boundary Value = 21	     System should NOT accept
7. Boundary Value = 49	     System should Not accept
8. Boundary Value =**50**	     System **should accept**
9. Boundary Value = 51	     System should  Not accept
10. Boundary Value = 99	     System should NOT accept
11. Boundary Value = **100**     System **should accept**
12. Boundary Value = 101	 System should NOT accept


# **State Transition diagram**
- State Transition Diagram.xmind - another file


# **Decision Table**
- Decision table.md - another file


# **Scenarios for ATM usage**
### **Test case 1: Withdrawal from ATM with receipt**
      1.Insert your card into Card Reader
      2.ATM menu will be shown
      3.Choose the desired amount
      4.On the next step you will be asked for a receipt
      5.Click the button with label "Yes"
      5.Enter PIN
      6.Take amount from the Cash Dispenser
      7.Take your receipt for transaction
      8.Show display message: "Do you want to continue?"
      9.Click the button with label "No"
      10.Take your card

### **Test case 2: Withdrawal from ATM without receipt**
      1.Insert your card into Card Reader
      2.ATM menu will be shown
      3.Choose the desired amount
      4.On the next step you will be asked for a receipt
      5.Click the button with label "No"
      5.Enter PIN
      6.Take amount from the Cash Dispenser
      7.Show display message: "Do you want to continue?"
      8.Click the button with label "No"
      9.Take your card      

### **Test case 3: Check your account balance from ATM with receipt**
      1.Insert your card into Card Reader
      2.ATM menu will be shown
      3.Choose the option "Check account balance"
      4.On the next step you will be asked for a receipt
      5.Enter your PIN
      6.Take receipt with current balance
      7.Show display message: "Do you want to continue?"
      8.Click the button with label "No"
      9.Take your card

### **Test case 4: Check your account balance from ATM without receipt**
      1.Insert your card into Card Reader
      2.ATM menu will be shown
      3.Choose the option "Check account balance"
      4.On the next step you will be asked for a receipt
      5.Click the button with label "No"
      5.Enter your PIN
      6.The current balance will be shown on the display
      7.Show display message: "Do you want to continue?"
      8.Click the button with label "No"
      9.Take your card
      


### **Test case 5:Deposit cash**
      1.Insert your card into Card Reader
      2.ATM menu will be shown
      3.Choose the option "Deposit"
      4.Select a deposit type
      5.Click  the button with label "cash"
      6.On the next step you will be asked for a receipt
      7.Enter your PIN
      8.Prepare cash continually
      9.Put the cash in the "Deposit slot"
      10. Show display message: "Please wait while we count your cash"
      11. Show display message: "Your cash has been accepted"
      12. Click the button with label "Deposit Complete"
      13. Show display message: "Do you want to continue ?"
      14. Click the button with label "No"
      15. Take your card    




