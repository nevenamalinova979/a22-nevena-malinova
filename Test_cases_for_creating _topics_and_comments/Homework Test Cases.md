# **Homework test cases forum**

Create test cases covering the following functionality of a forum:

-  ### **Comments**
-  ### **Creation of topics**

 ## **1.Title: Login in the forum**

   **Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)
2. Go to login section
3. Enter your credentials
4. Make sure you are log in

 ## **2.Title: Posting topic**

  **Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Entertype title of the topic or paste link

8. Enter a message in English or Bulgarian language

9. Click the button "Create topic"

10. Check the created topic in the list

## **3. Title: Posting topic with categorized section**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Entertype title of the topic or paste link

8. Enter a message in English or Bulgarian language

9. Click on uncategorized section and choose the category

10. Click the button "Create topic"

## **4. Title: Posting topic with optional tags**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Entertype title of the topic or paste link

8. Enter a message in English or Bulgarian language

9. Click on optional tags and choose the tag

10. Click the button "Create topic"

 ## **5. Title: Posting topic with categorized section and optional tags**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Entertype title of the topic or paste link

8. Enter a message in English or Bulgarian language

9. Click on uncategorized section and choose the category

10. Click on optional tags and choose the tag

11. Click the button "Create topic"

## **6. Title: Posting topic with image from your device**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Entertype title of the topic or paste link

8. Enter a message in English or Bulgarian language

9. Click on upload

10. Display a small window

11. Choose "From my device"

12. Click on "Upload" button

13. Click the button "Create topic"

## **7. Title: Posting topic with image from the web**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Entertype title of the topic or paste link

8. Enter a message in English or Bulgarian language

9. Click on upload

10. Display a small window

11. Choose "From the web"

12. Click on "Upload" button

13. Click the button "Create topic"

 ## **8. Title: Posting topic with message minimum 10 symbols**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Entertype title of the topic or paste link

8. Enter a message in English or Bulgarian language minimum 10 symbols

9. Click the button "Create topic"

## **9. Title: Posting topic with message maximum 200 symbols**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to the button "New topic"

6. Click on the button "New topic"

7. Enter type title of the topic or paste link

8. Enter a message in English or Bulgarian language maximum 200 symbols

9. Click the button "Create topic"

## **10. Title: Posting comment to topic**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to search button

6. Enter the search topic

7. Find the search topic

8. Click on search topic

9. Click on "Replay"

10. Display a small field

11. Enter your comment

12. Click the "Replay" button

## **11. Title: Deleting comment to topic**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to search button

6. Enter the search topic

7. Find the search topic

8. Click on search topic

9. Click on the "three dots"

10. Display additional menu

11. Click delete icon

 ## **12. Title: Editing comment to topic**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to search button

6. Enter the search topic

7. Find the search topic

8. Click on search topic

9. Click the "red pen" edit this comment

## **13. Title: Posting comment to topic with minimum 1 symbol**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to search button

6. Enter the search topic

7. Find the search topic

8. Click on search topic

9. Click on "Replay"

10. Display a small field

11. Enter your comment with minimum 1 symbol

12. Click the "Replay" button

## **14. Title: Posting comment to topic with maximum 200 symbols**

**Steps to reproduce:**

1. Go to [https://forum.telerikacademy.com/](https://forum.telerikacademy.com/)

2. Go to login section

3. Enter your credentials

4. Make sure you are log in

5. Go to search button

6. Enter the search topic

7. Find the search topic

8. Click on search topic

9. Click on "Replay"

10. Display a small field

11. Enter your comment with maximum 200 symbols

12. Click the "Replay" button