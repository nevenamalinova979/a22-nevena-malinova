# **Test plan**

  ## **1.Introduction**

-   Тest plan for testing a forum 


  ## **2.Scope**

-   Topic creation functionalities


  ## **3.Types of testing:**

-   Smoke testing

-   Functional testing

-   Usability testing


  ## **4.Features to Be Tested**

-   Creating test cases

-   Generating testing data

-   Test cases automation

-   Test cases execution – manual and automation

-   Reporting issues

-   Creating a test report

-   Creating an issue report



  ## **5.Features not to be tested**

-   Comments’ functionalities

-   User management’s functionalities

-   Administrator role’s functionalities


  ## **6.Schedule and timelines**

    Testing time – 9 working days

-   Test cases creation, testing data – 3 day

-   Test cases automation – 2 days

-   Test Execution (manual and automated), 
    reporting   issues – 2 days

-   Test and issue reports creation – 2 day

   ## **7.Environmental needs:**

-   Windows 10 laptop with installed:

    -   Chrome
    
    -   Firefox

    -   IntelliJ Idea Ultimate

  ## **8.Testing tools:**

-   Maven

-   Selenium WebDriver

-   Junit

-   Log4j

  ## **9.Exit criteria:**

-   80% of Priority2 tests pass

-   100% of Priority1 tests pass

-   Time ran out
