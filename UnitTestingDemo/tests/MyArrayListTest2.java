import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MyArrayListTest2 {

    private MyList<Integer> list;
    @BeforeEach
    public void init() {
        list = new MyArrayList<>(); // Arrange
    }

    @Test
    public void get_should_returnRightElement_when_IndexIsValid(){
        list.add(1);
        list.add(2);
        // Act
        Integer result = list.get(0);
        // Assert
        Assertions.assertEquals(1,result);
    }

    @Test
    public void get_should_throw_when_indexIsBelowZero(){
        // Arrange
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class, ()-> list.get(-1));
    }

    @Test
    public void get_should_throw_when_indexIsAboveTheUpperBound(){
        // Arrange
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.get(0));
    }
    @Test
    public void get_should_when_methodReturnTheRightLastElement(){
        list.add(1);
        list.add(2);
        // Act
        Integer result = list.getLast();
        // Assert
        Assertions.assertEquals(2,result);
    }
    @Test
    public void get_should_when_collectionIsEmptyByLasElement(){
        //Arrange
        // Act
        // Integer result = list.getLast();
        // Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> list.getLast());
    }
    @Test
    public void get_should_when_methodReturnRightFirstElement(){
        // Arrange  public void init()
        list.add(1);
        list.add(2);
        //Act
        Integer result = list.getFirst();
        //Assert
        Assertions.assertEquals(1,result);
    }
    @Test
    public void get_should_when_collectionIsEmptyByFirstElement(){
        // Arrange public void init()
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,() -> list.getFirst());
    }
}
