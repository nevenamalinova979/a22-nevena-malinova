
import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class MyArrayListTests {
//    @Test
//    public void test() {
//        Assertions.assertEquals("42","42");
//    }
//-----------------------------------------------------------------------------------------------------
    private MyList<Integer> list;

    @BeforeEach
    public void init() {
        // Arrange
        list = new MyArrayList<>(); // преди всеки едни тест ще ни инициализира нов лист, т.е да го зачиства
    }
//------------------------------------------------------------------------------------------------------------
// 1. трябва да ги подготвим средата, но първо да кръстим теста, според избраната конвенция
    @Test
    public void get_should_returnRightElement_when_IndexIsValid(){
                     // Arrange
      // MyArrayList<Integer> list = new MyArrayList<>(); - махаме го защото  @BeforeEach public void init()
       list.add(1);
       list.add(2);
                     // Act
        Integer result = list.get(0);
                    // Assert
        Assertions.assertEquals(1,result);
    }
//---------------------------------------------------------------------------------------------------------
    @Test
    public void get_should_throw_when_indexIsBelowZero(){
        // Arrange
       /* MyArrayList<Integer> list = new MyArrayList<>(); /* трябва да повторим код и няма смисъл да добавяме елементи,
тъй като ще дибавяме невалидни резултати, но ще ни трябва едни MyArrayList<Integer> list = new MyArrayList<>();,
за да извикаме мтода върху него, инече ще ни хвърли no pointer exception. За да не се поваря код ще се възползваме от една
 от анотациите, а именно *@BeforeEach  и следователно ще махнем инициализацията  MyArrayList<Integer> list = new MyArrayList<>(); */
        // Act Assert - ще будат на едно и също място
        Assertions.assertThrows(IndexOutOfBoundsException.class, ()-> list.get(-1));
    // ()-> list.get(-1)) - някакъв executable
// ()-> A lambda expression is a short block of code which takes in parameters and returns a value.
// Lambda expressions are similar to methods, but they do not need a name and they can be implemented right in the body of a method.
// The simplest lambda expression contains a single parameter and an expression:
// parameter -> expression
// начин да окажем, че искаме да лист да извикаме метода get
    }
//---------------------------------------------------------------------------------------------------------------------

    @Test
    public void get_should_throw_when_indexIsAboveTheUpperBound(){
        // Arrange:  list = new MyArrayList<>();
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.get(0));
    }
    //   private void checkIndexIsValid(int index) {
    //        if (index < 0 || index > getUsedPositions()) {
    //            throw new IndexOutOfBoundsException();
    //        } - ako махнем = тозо тест ще хвърли грешка
//-----------------------------------------------------------------------------------
    @Test  // Does not catch the bug
    public void get_should_throw_when_indexIsAboveTheUpperBoundPlusOne(){
        // Arrange:  list = new MyArrayList<>();
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.get(1));
    }
 //--------------------------------------------------------------------------------------------------
}
