import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YouTube {
    private static final String YOUTUBE = "https://www.youtube.com/";
    private static final String YOUTUBE_VIDEO = "https://www.youtube.com/watch?v=e_04ZrNroTo";
    private WebDriver driver;

    @BeforeClass
    public static void testClassInitialization(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Nevena\\Documents\\JAVA\\Module 3\\ChromeDriver\\chromedriver.exe");
    }
    @Before
    public void testInitialization(){
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get(YOUTUBE);
    }
    @After
    public void testCleanUp(){
        driver.close();
    }


    @Test
    public void youTubeTest() {
        agreement();
        selectVideo();
        pauseVideo();
        fullScreen();
        fullScreenExit();
    }

    private void agreement() {
        WebDriverWait wait = new WebDriverWait(driver, 15);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@id, 'dismiss-button')]")));
        WebElement signInButton = driver.findElement(By.xpath("//*[contains(@id, 'dismiss-button')]"));
        signInButton.click();

        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@id, 'introAgreeButton')]")));
        WebElement agreeButton = driver.findElement(By.xpath("//*[contains(@id, 'introAgreeButton')]"));
        agreeButton.click();
        driver.switchTo().parentFrame();
    }

    private void selectVideo() {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='search']")));
        WebElement search = driver.findElement(By.xpath("//input[@id='search']"));
        search.sendKeys("the wheels on the bus", Keys.ENTER);
        WebElement video = driver.findElement(By.xpath("//a[@id='video-title']//yt-formatted-string[contains(text(),'Wheels on the Bus | CoComelon Nursery Rhymes & Kid')]"));
        video.click();
        Assert.assertEquals(YOUTUBE_VIDEO, driver.getCurrentUrl());
    }

    private void pauseVideo() {
        WebElement player = driver.findElement(By.xpath("//body"));
        player.sendKeys("k");
        WebElement button = driver.findElement(By.xpath("//*[contains(@class,'ytp-play-button')] "));
        String title = button.getAttribute("title");
        Assert.assertEquals("Play (k)", title);
    }

    public void fullScreen() {
        WebElement player = driver.findElement(By.xpath("//body"));
        player.sendKeys("f");
        WebElement button = driver.findElement(By.xpath("//*[contains(@class,'ytp-fullscreen-button')] "));
        String title = button.getAttribute("title");
        Assert.assertEquals("Exit full screen (f)", title);
    }

    public void fullScreenExit() {
        WebElement player = driver.findElement(By.xpath("//body"));
        player.sendKeys("f");
        WebElement button = driver.findElement(By.xpath("//*[contains(@class,'ytp-fullscreen-button')] "));
        String title = button.getAttribute("title");
        Assert.assertEquals("Full screen (f)", title);
    }
}
